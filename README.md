# The Age of Legends

# Notice #
This repo is huge. Sorry about that, perhaps I'll seperate the content folder into
a seperate download?

## Build Instructions

* Have a copy of Unreal Engine 4 updated to the latest stable release.
* Double click the .uproject file to open the editor.
* Right click the uproject file and click "Generate Build Files".
* That's it. :)