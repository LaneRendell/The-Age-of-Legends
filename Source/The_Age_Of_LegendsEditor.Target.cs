// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class The_Age_Of_LegendsEditorTarget : TargetRules
{
	public The_Age_Of_LegendsEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "The_Age_Of_Legends" } );
	}
}
