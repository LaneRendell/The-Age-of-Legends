// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class The_Age_Of_LegendsTarget : TargetRules
{
	public The_Age_Of_LegendsTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "The_Age_Of_Legends" } );
	}
}
